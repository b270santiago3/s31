let http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if (request.url === '/login') {
  		response.writeHead(200, {'Content-Type': 'text/plain'});
    	response.end('Welcome to the login page.');

  	} else if (request.url !== '/') {
    	response.writeHead(404 , {'Content-Type': 'text/plain'});
    	response.end('Im sorry the page you are looking for cannot be found.');

  	} else {
    	response.end('Hello Mam Daisy!');
  	}

});

server.listen(port, () => {
  console.log(`Server is running at localhost:3000 ${port}`);
});